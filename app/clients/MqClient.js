const stompit = require('stompit');
const config = require('../config');

class MqClient {

    constructor() {
        this.defaultTopic = 'defaultTopic';
        this.channelFactory = this.conn();
    }

    conn() {
        const servers = [
            {
                "host": config.mq.host,
                "port": config.mq.port,
                "connectHeaders": {
                    "host": "/",
                    "login": process.env.ACTIVEMQ_ADMIN_LOGIN || config.mq.login,
                    "passcode": process.env.ACTIVEMQ_ADMIN_PASSWORD || config.mq.passcode,
                    "heart-beat": "100,100"
                }
            }
        ];
        console.log(`Connecting to queue engine: ${config.mq.host}:${config.mq.port}`);

        try {
            const connections = new stompit.ConnectFailover(servers);

            connections.on('error', function (error) {
                console.log(`Couldn't connect to ${error.connectArgs.host}:${error.connectArgs.port}: ${error.message}`);
            });

            return new stompit.ChannelFactory(connections);
        } catch (e) {
            console.log(`Trouble connecting to queue engine: ${e.stack}`);
        }
    }

    async send(obj, destination) {
        return new Promise((resolve, reject) => {
            this.channelFactory.channel(function (error, channel) {

                const sendHeaders = {
                    destination: destination || this.defaultTopic
                };

                channel.send(sendHeaders, JSON.stringify(obj), function (error) {
                    console.log(`Sending order to topic ${sendHeaders.destination}`);

                    if (error) {
                        console.log(`Couldn't publish message to topic, error: ${error.stack}`);
                        return reject();
                    }
                    console.log(`Message sent to topic ${sendHeaders.destination} successfully.`);
                    return resolve();
                });
            });
        });
    }
}

module.exports = new MqClient();
