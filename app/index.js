console.log("Index init");
const oneService = require('./services/oneServices');
const twoService = require('./services/twoServices');

(async ()=> {
    await oneService.sendOneMessage();
    await twoService.subscribeTo();
})();
