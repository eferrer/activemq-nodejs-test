const MqClientClass = require('../clients/MqClient');
const config = require('../config');

const mqClient = MqClientClass.getInstance();

exports.sendOneMessage = async () => {

    const objToSend = {
        id: 1,
        name: 'Emilio',
        lastName: 'Ferrer',
    };

    await mqClient.send(objToSend, config.topicOne);
};
