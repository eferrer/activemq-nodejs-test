const MqClientClass = require('../clients/MqClient');
const config = require('../config');

const mqClient = MqClientClass.getInstance();

exports.subscribeTo = (destination) => {

    mqClient.channelFactory.channel(function (error, channel) {
        const headers = {
            destination: destination || config.topicOne,
            ack: 'client-individual'
        };

        channel.subscribe(headers, function (error, message, subscription) {
            console.log(`Message received on topic ${headers.destination}`);

            if (error) {
                console.log(`Subscription error: ${error.message}`);
                return;
            }

            let dataFromMq;

            message.readString('utf8', async function (error, body) {
                if (error) {
                    console.log(`Couldn't read message from topic: ${error.message}`);
                    return;
                }
                channel.ack(message);

                try {
                    console.log(`Message read from topic: ${body}`);
                    dataFromMq = JSON.parse(body);
                } catch (e) {
                    console.log(`Problems parsing message gotten from topic or doing acknowledgement: ${e.stack}`);
                }
            });
        });
        console.log(`Subscription to topic: ${headers.destination}`);
    });
};
