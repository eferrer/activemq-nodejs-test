const fs = require('fs');

console.log('Cargando configuraciones');
const config = JSON.parse(
    fs.readFileSync('./app/config/config.json', 'utf8')
);

module.exports = config;
