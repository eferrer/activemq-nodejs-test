# activemq-nodejs-test

### Pasos para probar Active MQ:

#### 1. Levantar ActiveMQ en un contenedor
`
docker run --name='activemq' -itd  -e 'ACTIVEMQ_CONFIG_MINMEMORY=512' \
  -e 'ACTIVEMQ_CONFIG_MAXMEMORY=2048' -p 8161:8161 -p 61616:61616 -p 61613:61613 \
   webcenter/activemq:5.14.3
`

#### 2. Ejecución
`
npm run server
`

#### 3. Enviar mensajes a la cola
`
node test.js
`
