const mqClient = require('./app/clients/MqClient');


const objToSend = {
    id: 2,
    name: 'Pablo',
    lastName: 'Perez',
};

(async () => {
    await mqClient.send(objToSend, 'testTopic')
})();
